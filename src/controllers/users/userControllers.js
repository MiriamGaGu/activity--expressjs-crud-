// definir funciones para http request
// const fs = require('fs')
const users = require('../../../data.json')

const controllers = {
    index: (req, res) => {
        console.log(req.params)
        res
            .status(200)
            .json({
                data: users
            })
    },
    find: (req, res) => {
        // console.log(typeof req.params.id)
        const queryId = req.params.id

        const user = users.data.filter(user => {
            return user.id.toString() === queryId
        })
        // console.log(user)
        res
            .status(200)
            .json({
                data: user
            })
    }


}

//exportar
module.exports = controllers;
