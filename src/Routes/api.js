//definir rutas

const { Router } = require('express')

const app = Router()

const Users = require("../controllers/users/userControllers")

app.get('/users', Users.index)
app.get('/users/:id', Users.find)
app.get('users/:id/login', Users.login)
app.get('users/:id/profile', Users.profile)



module.exports = app;
